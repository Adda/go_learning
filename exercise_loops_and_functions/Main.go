package main

import (
    "fmt"
)

func Sqrt(x float64) float64 {
    prev_z := 1.

    for z := x; z*z > x; z -= (z*z - x) / (2*z) {
        prev_z = z
        fmt.Println(z, x)
    }

    return prev_z
}

func main() {

    square_root := Sqrt(1)
    fmt.Printf("%v %T\n\n", square_root, square_root)

    square_root = Sqrt(2)
    fmt.Printf("%v %T\n\n", square_root, square_root)

    square_root = Sqrt(9)
    fmt.Printf("%v %T\n\n", square_root, square_root)

    square_root = Sqrt(25)
    fmt.Printf("%v %T\n\n", square_root, square_root)

    square_root = Sqrt(100)
    fmt.Printf("%v %T\n\n", square_root, square_root)

    square_root = Sqrt(400)
    fmt.Printf("%v %T\n\n", square_root, square_root)
}
